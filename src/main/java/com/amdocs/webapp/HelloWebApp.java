package com.amdocs.webapp;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWebApp extends HttpServlet {

	private BasicCalculator calc;

	private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	         throws ServletException, IOException {
         resp.setContentType("text/html");
         resp.getWriter().println("<html>");
        resp.getWriter().println("<head>");
        resp.getWriter().println("<title>Zantis HTML Page</title>");
        resp.getWriter().println("<style>");
        resp.getWriter().println("body { font-family: Arial, sans-serif; background-color: #f4f4f4; color: #333; }");
        resp.getWriter().println("h1 { color: #4CAF50; }");
        resp.getWriter().println(".pagination { display: inline-block; }");
        resp.getWriter().println(".pagination a { color: black; float: left; padding: 8px 16px; text-decoration: none; transition: background-color .3s; }");
        resp.getWriter().println(".pagination a.active { background-color: #4CAF50; color: white; }");
        resp.getWriter().println(".pagination a:hover:not(.active) { background-color: #ddd; }");
        resp.getWriter().println("</style>");
        resp.getWriter().println("</head>");
        resp.getWriter().println("<body class=\"center-justified\" >");
        resp.getWriter().println("<h1>Hello Limassol!</h1>");
        resp.getWriter().println("<p>Java Project for DevOps Hands-On Workshop change for the screenshot</p>");
                resp.getWriter().println("<img src=\"https://scontent-mrs2-1.xx.fbcdn.net/v/t39.30808-1/387729567_10230947012563196_6338931632038212267_n.jpg?stp=dst-jpg_p160x160&_nc_cat=100&ccb=1-7&_nc_sid=5f2048&_nc_ohc=mSQYI2i_wgkQ7kNvgH8w9_v&_nc_ht=scontent-mrs2-1.xx&oh=00_AYByXjfvlq82Keu6DltGy4TwJ-wSAUpwOCAx1tf8rTFP_w&oe=6670668E\" >");
        resp.getWriter().println("</body>");
        resp.getWriter().println("</html>");
        }


	public HelloWebApp() {
		this.calc = null;
	}

	public HelloWebApp(BasicCalculator calc) {
		this.calc = calc;
	}

	public int avg(int a, int b) {
	    return calc.add(a, b)/2;
	}

}
